const TodoController = require('../services/TodosController');

module.exports = (app) => {
  app.route('/todos')
    .get(TodoController.fetchTodos)
    .post(TodoController.addTodos);
  app.route('/todos/:id')
    .put(TodoController.updateTodo)
    .delete(TodoController.deleteTodo);
}
