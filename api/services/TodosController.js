//@ts-check
const TodoModel = require('../models/Todo');

module.exports = {

  /**
 * @type function
 */
  fetchTodos(req, res) {
    TodoModel.find()
      .then(data => {
        res.json(data)
      })
      .catch(err => console.log(err))
  },

  addTodos(req, res) {
    TodoModel.findOne({ title: req.body.title })
      .then(todo => {
        if (todo) {
          res.json({
            success: false,
            data: {
              message: 'Todo already exist'
            }
          })
        } else {
          let newTodo = new TodoModel(req.body);
          newTodo.save()
            .then(data => {
              res.json({
                success: true,
                data: data
              })
            })
            .catch(err => console.log(err));
        }
      })
      .catch(err => console.log(err))
  },

  updateTodo(req, res) {
    TodoModel.replaceOne({ _id: req.params.id }, req.body)
      .then(updatedTodo => {
        res.json({
          success: true,
          data: updatedTodo
        })
      })
      .catch(err => console.log(err))
  },

  deleteTodo(req, res) {
    TodoModel.deleteOne({ _id: req.params.id })
      .then(data => {
        res.json({
          success: true,
          data: data
        })
      })
      .catch(err => console.log(err))
  }
}