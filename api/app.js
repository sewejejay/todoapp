const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

//init environment variable
require('dotenv').config()

// init app
const app = express();

// Init DB
require('../api/config/db');

/** 
 * Init Middlewares
 */
app.use(cors({
  origin: ['http://localhost:4200']
}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("combined"));

//import modules
const TodoModule = require('./routes/todos');

//Init modules
TodoModule(app)

module.exports = app;
