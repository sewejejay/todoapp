const mongoose = require('mongoose');

const TodoSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean
  }
})

module.exports = mongoose.model('todos', TodoSchema);